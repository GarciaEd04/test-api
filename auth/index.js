
const jwt = require('jsonwebtoken')

class Authentication {
    static setToken (user) {
        const payload = {
            check: true,
            user
        }

        const token = jwt.sign(payload, 'test', {
            expiresIn: 86400
        })

        return token
    }
}

module.exports = Authentication
