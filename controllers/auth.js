
const auth = require('../auth/index')
const User = require('../models/user')
const PasswordUtils = require('../utils/password_utils')
class AuthRepository {
    static async login (username, password) {
        const user = await User.findOne({ username });
        if (!user) {
            return { error: 'User not found' }
        }

        const match = await PasswordUtils.compare(password, user.password);
        if (!match) {
            return { error: 'Invalid password' }
        }

        if (!user.active) {
            return { error: 'User disabled' }
        }

        const userInfo = {
            id: user.id,
            email: user.email,
            name: user.name,
            role: user.role,
            lang: user.lang,
            lastActivity: user.last_activity
        };
        const token = auth.setToken(userInfo);

        return { token, user: userInfo }
    }
}

module.exports = AuthRepository
