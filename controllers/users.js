
const User = require('../models/user');
const PasswordUtils = require('../utils/password_utils');

class UsersController {
    static listAll () {
        return User.find()
    }

    static async create (data) {
        data.password = await PasswordUtils.generatePassword(data.password);
        data.active = true;
        const user = new User(data);
        await user.save();
        return user
    }

    static fetchByEmail (email) {
        return User.findOne({ email })
    }

    static async update (email, data) {
        const user = await this.fetchByEmail(email);
        console.log(user, email)

        user.name = data.name
        user.email = data.email

        if (data.password) {
            data.password = await PasswordUtils.generatePassword(data.password);
            user.password = data.password
        }
        const updatedUser = await user.save();
        console.log(updatedUser)
        return updatedUser
    }

    static async toggleUser (email, active) {
        const user = await User.findOneAndUpdate({email}, {active}, {new: true});
        return user
    }
}

module.exports = UsersController;
