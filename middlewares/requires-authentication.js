
const express = require('express')
const jwt = require('jsonwebtoken')
const requiresAuthentication = express.Router()

requiresAuthentication.use(async (req, res, next) => {
    const token = req.headers['access-token']

    if (token) {
        jwt.verify(token, 'test', (err, decoded) => {
            if (err) {
                res.status(400)
                return res.json({ error: 'Invalid token!' })
            } else {
                req.decoded = decoded
                next()
            }
        })
    } else {
        res.status(400)
        res.send({
            error: 'Token no provided.'
        })
    }
})

module.exports = requiresAuthentication
