const express = require('express');
const router = express.Router();
const asyncHandler = require('express-async-handler');
const AuthController = require('../controllers/auth');

router.post('/login', asyncHandler(async (req, res, next) => {
    const response = await AuthController.login(req.body.username, req.body.password)
    if (response.error) {
        res.status(400)
    }
    res.json(response)
}));

module.exports = {
    router
}
