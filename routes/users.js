const express = require('express');
const router = express.Router();
const basePath = '/users';
const asyncHandler = require('express-async-handler');
const UsersController = require('../controllers/users');

router.post(`${basePath}/create`, asyncHandler(async (req, res, next) => {
    const user = await UsersController.create(req.body.user);
    if (user.error) {
        res.status(400)
    }
    res.json(user)
}))

router.post(`${basePath}/update/:email`, asyncHandler(async (req, res, next) => {
    const user = await UsersController.update(req.params.email, req.body.user);
    res.json(user)
}))

router.post(`${basePath}/toggle/:email`, asyncHandler(async (req, res, next) => {
    res.json(await UsersController.toggleUser(req.params.email, req.body.active))
}))

router.get(`${basePath}/list`, asyncHandler(async (req, res, next) => {
    const users = await UsersController.listAll();
    res.json(users)
}))

router.get(`${basePath}/:email`, asyncHandler(async (req, res, next) => {
    const user = await UsersController.fetchByEmail(req.params.email);
    res.json(user)
}))

module.exports = {
    router
}
