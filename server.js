require('dotenv').config()

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const users = require('./routes/users');
const auth = require('./routes/auth');
const cors = require('cors');
const mongoose = require('mongoose');
const requiresAuthentication = require('./middlewares/requires-authentication');

mongoose.connect(process.env.DATABASE_URL, { useNewUrlParser: true })
const db = mongoose.connection
db.on('error', (error) => console.error(error))
db.once('open', () => console.log('connected to database'))

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/auth',
    auth.router
)

app.use('/api',
    requiresAuthentication,
    users.router
);

app.listen(8000, () => {
    console.log('Server started!')
});
