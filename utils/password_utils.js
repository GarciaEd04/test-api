
const bcrypt = require('bcryptjs')
const saltRounds = 10

class PasswordUtils {
    static generatePassword (password) {
        return bcrypt.hash(password, saltRounds)
    }

    static compare (password, userPassword) {
        return bcrypt.compare(password, userPassword)
    }
}

module.exports = PasswordUtils
